/**
 * Задача 1.
 *
 * Дан базовый класс робота-уборщика.
 *
 * Задача:
 * Добавьте роботу функционал употребления энергии:
 * - при начале уборки уровень энергии должен уменьшиться;
 * - в расчёте использовать внутренний коэффициент ENERGY_CONSUMPTION.
 *
 * Затем добавьте роботу публичный метод stop() для остановки процесса уборки.
 * В если уборка остановлена раньше времени завершения,
 * onReady сработать не должен, а также нужно вывести другое сообщение.
 *
 * Условия:
 * - заданную форму конструктора включая его параметры менять нельзя — можно только дополнять и переносить в методы обработки события;
 * - использовать функцию clearTimeout для завершения уборки;
 * - идентификатор таймера нужно хранить в приватной переменной конструктора timerId.
 * - значение INITIAL_ENERGY необходимо получить из инпута name="energy"
 * - значение INITIAL_SQUARE необходимо получить из инпута name="square"
 * - работу робота необходимо запускать по нажатию на кнопку в форме — Начать уборку. Обратит внимание, что форма не должна перезагружать страничку
 * - все информационные сообщения необходимо выводить в HTML в элемент — p. Заменить console.log на код для вывода информации в HTML
 *
 * Приблизительный план действий:
 * 1. Добавить идентификатор в форму, который будет использоваться для добавления обработчика onsubmit
 * 2. Из инпутов извлечь необходимую информацию и передать в конструктор CleanerRobot
 * 3. При подтверждении формы необходимо создать экземпляр CleanerRobot и запустить его работу при помощи метода clean
 * 4. Добавить селектор p элементу и использовать его для отображения текста из методов onReady и clean
 * 5. После завершения работы с DOM приступить к реализации логики робота
 *
 * Подсказки:
 * - в HTML допускается добавление идентификаторов для более удобной работы с дом
 * - вам могут потребоваться следующие методы и свойства — innerHTML, getElementById, onsubmit
 */

function CleanerRobot(
    initialEnergy = 0 /* Изначальный заряд батареи робота */,
    cleaningSquare /* Площадь для уборки в метрах. */,
) {
    let energy = initialEnergy;
    let timerId = 0;
    let cleaningEnd = true;
    const ENERGY_CONSUMPTION = 1; /* Расход энергии: 1% батареи на 1 час работы. */
    const CLEANING_SPEED = 10; /* Скорость уборки: 10 квадратных метров в час. */

    const getCleaningTime = () => cleaningSquare / CLEANING_SPEED;
    const onReady = () => {
        if(!cleaningEnd) {
            return false;
        }
        const message = `Уборка завершена. Осталось заряда батареи: ${energy - (ENERGY_CONSUMPTION * getCleaningTime())}.`;
        displayMessage(message);
    };
    this.clean = () => {
        const cleaningTime = getCleaningTime();
        const message = `Начинаю процесс уборки. Время уборки: ${cleaningTime} часов.`;
        displayMessage(message);

        /* Для удобства время уборки сокращено до формата 1 час = 1 секунда */
        timerId = new Date();
        setTimeout(onReady, cleaningTime * 1000);
    };
    this.stop = () => {
        clearTimeout()
        const timerCount = (new Date() - timerId) / 1000;
        let energyLeft = energy - (ENERGY_CONSUMPTION * timerCount);
        const message = `Спустя ${timerCount.toFixed(2)} секунды: Уборка завершена. Осталось заряда батареи: ${energyLeft.toFixed(2)}`;
        displayMessage(message);
        return false;
    };
    const clearTimeout = () => {
        cleaningEnd = false;
    };
    // Решение
}
const startButton = document.querySelector('input[class="btn btn-primary"]');
startButton.setAttribute('id', 'start cleaning');
const startWork = document.querySelector('form');
const endButton = document.createElement('input');
endButton.setAttribute('type', 'submit');
endButton.setAttribute('class', 'btn btn-primary');
endButton.setAttribute('value', 'Закончить уборку');
endButton.setAttribute('id', 'end cleaning');
startWork.appendChild(endButton);

startButton.onclick = (event) => {
    while (startWork.parentElement.children.length > 1){
        startWork.parentElement.removeChild(startWork.parentElement.lastChild);
    }
    const INITIAL_ENERGY = document.getElementById('energyInput').value;
    const INITIAL_SQUARE = document.getElementById('squareInput').value;
    const cleanerRobot = new CleanerRobot(INITIAL_ENERGY, INITIAL_SQUARE);
    cleanerRobot.clean(); /* Начинаю процесс уборки. Время уборки: 4.5 часов. */
        endButton.onclick = (event) => {
            cleanerRobot.stop();
            endButton.disabled = true;  /* отключаем кнопку, так как не логично отсанавливать несколько раз робота. */
            return false;
    };
    endButton.disabled = false;  /* включаем кнопку, так как новые вводные или робот запущен снова c теме же значениями */
    return false;
};

const displayMessage = (message) => {
    const p = document.createElement('p');
    p.setAttribute('style', 'margin-top: 50px;');
    p.textContent = message;
    startWork.parentElement.appendChild(p);
};

setTimeout(() => {
    cleanerRobot.stop(); /* Спустя 1 секунду: Уборка завершена досрочно. Осталось заряда батареи: 45.5. */
}, 1000);
