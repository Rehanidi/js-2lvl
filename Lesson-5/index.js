/*
# Задача 1

Доработать класс `DB` который будет имплементировать `CRUD` модель.
В качестве структуры данных использовать `Map`.

Метод `create`:
    - принимает объект и валидирует его, в случае невалидности объекта – генерирует ошибку.
    - возвращает `id` при создании пользователя генерирует уникальный `id`, который является ключом для объекта пользователя в структуре `Map`
Метод `read`:
    - принимает идентификатор пользователь
    - если такого пользователя нет возвращать `null`
    - если есть — возвращать объект пользователя с полем `id` внутри объекта.
    - если в метод `read` передать не строку, или не передать параметр вообще — генерировать ошибку.
Метод `readAll`:
    - возвращает массив пользователей
    - генерировать ошибку если в метод `readAll` передан параметр
Метод `update`:
    - обновляет пользователя
    - принимает 2 обязательных параметра
    - генерирует ошибку если передан несуществующий `id`
    - генерирует ошибку если передан `id` с типом не строка
    - генерирует ошибку если второй параметр не валидный
Метод `remove`:
    - удаляет пользователя
    - Генерировать ошибку если передан в метод `delete` несуществующий или невалидный `id`

Обратите внимание!
    - Вам может потребоваться дописать код в событиях onsubmit и onclick для того что бы обрабатывать и выводить сообщения об ошибках
*/

// РЕШЕНИЕ
class DB {
    id = 0;
    customers = new Map();
    create(obj) {
        if (obj.name === ''){
            alert('Enter name');
            return false;
        } else if (obj.age === ''){
            alert('Enter age');
            return false;
        } else if (obj.country === ''){
            alert('Enter country');
            return false;
        }
        this.id++;
        obj.id = this.id;
        this.customers.set(this.id, obj);
        return this.id;
    }
    read (uid) {
        if (typeof uid !== 'string' || uid === ''){
            alert(Error.name);
        } else if (!this.customers.has(Number(uid))){
            return null;
        }
        return {
            name: this.customers.get(Number(uid)).name,
            age: this.customers.get(Number(uid)).age,
            country: this.customers.get(Number(uid)).country,
            salary: this.customers.get(Number(uid)).salary,
            id: this.customers.get(Number(uid)).id
        };
    }
    readAll () {
        if (arguments.length > 0){
            alert(Error.name);
        }
        return this.customers;
    }
    update (uid, { name, salary }) {
        if (this.customers.has(Number(uid)) !== true){
            alert('Wrong id!');
            return false;
        }
        if (typeof uid !== 'string'){
            alert('Wrong id type!');
            return false;
        }
        if (name === ''){
            alert('Enter name');
            return false;
        }
        if (salary === ''){
            alert('Enter salary');
            return false;
        }
        let customer = this.customers.get(Number(uid));
        customer.name = name;
        customer.salary = salary;
    }
    remove (uid) {
        if (typeof uid !== 'string'){
            alert('Wrong id type!');
            return false;
        }
        this.customers.delete(Number(uid));
    }
}
// РЕШЕНИЕ

// const person = {
//   name: "Pitter", // обязательное поле с типом string
//   age: 21, // обязательное поле с типом number
//   country: "ua", // обязательное поле с типом string
//   salary: 500 // обязательное поле с типом number
// };
// const db = new DB();
// const id = db.create(person);
// const customer = db.read(id);
// const customers = db.readAll(); // массив пользователей
// db.update(id, { age: 22 }); // id
// db.remove(id); // true

// ПРОВЕРКА
const db = new DB();

const regForm = document.getElementById('regForm');
const updateForm = document.getElementById('updateForm');
const searchForm = document.getElementById('searchForm');
const removeForm = document.getElementById('removeForm');
const alertP = document.getElementById('alert');
const readAllBtn = document.getElementById('readAll');
const list = document.getElementById('list');

regForm.onsubmit = (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const name = formData.get('name');
    const age = formData.get('age');
    const country = formData.get('country');
    const salary = formData.get('salary');

    const person = {
        name,
        age,
        country,
        salary
    };

    const id = db.create(person);

    alertP.innerHTML = `Сотрудник ${name} успешно добавлен. Идентификатор сотрудника ${id}.`;
}

searchForm.onsubmit = (event) => {
    event.preventDefault();
    list.innerHTML = null;

    const formData = new FormData(event.target);
    const uid = formData.get('uid');

    const customer = db.read(uid);
    const worker = getWorker(customer);

    list.insertAdjacentHTML('afterbegin', worker);
}

updateForm.onsubmit = (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const uid = formData.get('uid');
    const name = formData.get('name');
    const salary = formData.get('salary');

    db.update(uid, { name, salary });

    alertP.innerHTML = `Данные сотрудника ${uid} успешно обновлены.`;
}

removeForm.onsubmit = (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const uid = formData.get('uid');
    const result = db.remove(uid);

    if (result) {
        alertP.innerHTML = `Сотрудник с идентификатором ${uid} успешно удалён.`;

        return;
    }

    alertP.innerHTML = `При удалении сотрудника с идентификатором '${uid}' произошла ошибка.`;
}

readAllBtn.onclick = () => {
    const customers = db.readAll();
    list.innerHTML = null;
    let customersHTML = '';

    customers.forEach((customer) => {
        customersHTML += getWorker(customer);
    });

    list.insertAdjacentHTML('afterbegin', customersHTML);
}
